const ERR_UNAUTHORIZED = "UNAUTHORIZED";
const ERR_UNKNOWN = "UNKNOWN ERROR";

module.exports = (r) => {

  return (req, res, next) => {
    if (req.headers.authorization === undefined) {
      res.status(403).send(ERR_UNAUTHORIZED);
      return;
    }
    
    const split = req.headers.authorization.split(' ');

    if (split.length != 2) {
      res.status(403).send(ERR_UNAUTHORIZED);
      return;
    }

    const token = split[1];

    r.table('tokens')
      .getAll(token, { index: 'token' })
      .map((doc) => {
        return {
          id: doc('id'),      
          usageCount: doc('usageCount'),
          user: r.table('users').get(doc('user_id')).without(['password', 'perishableToken']),
          organization: r.table('organizations').get(doc('organization_id'))
        };
      })
      .then((results) => {        
        if (results.length == 1) {        
          const record = results[0];
          record.token = token;          
          r.table('tokens')
            .get(record.id)
            .update({
              usageCount: record.usageCount + 1,
              lastUsed: new Date()
            })
            .then((result) => {              
              req.auth = record;
              next();
            })
            .catch((error) => {
              res.status(500).send(ERR_UNKNOWN);
            });
        } else {
          res.status(403).send(ERR_UNAUTHORIZED);
        }
      })
  }
};