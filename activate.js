const _ = require('lodash');
const fs = require('fs');
const Handlebars = require('handlebars');

const ERR_MISSING_TOKEN = 'Missing token';
const ERR_INVALID_TOKEN = 'Invalid token';

module.exports = (r, email, _settings) => {

  const settings = _.merge({    
    site: {
      name: 'NAME THIS SITE!',                                          // used for templates
      url: 'http://localhost:8000'                                      // used for link generation
    },
    emails: {      
      active: {
        subject: 'Your account is now fully active',
        template: Handlebars.compile(fs.readFileSync(__dirname + '/templates/active.html', 'utf-8'))
      }
    }
  }, _settings);

  const notify = (params) => {
    return new Promise((resolve, reject) => {
      const data = {
        params: params,
        site: {
          name: settings.site.name
        }
      }
      const html = settings.emails.active.template(data);
      const message = {
        text: "",
        from: settings.email.from,
        subject: settings.emails.active.subject,
        to: params.email,
        attachment: [
          { data: html, alternative: true }
        ]
      };
      email.send(message, (error, message) => {
        if (error) {
          reject(new Error(ERR_EMAIL_NOTIFICATION));
        } else {
          resolve(params);
        }
      });
    });    
  };

  const fetch = (token) => {
    return r
      .table('users')
      .getAll(token, { index: 'perishableToken' })
      .then((results) => {
        if (results.length == 1) {
          return results[0];
        } else {
          throw new Error(ERR_INVALID_TOKEN);
        }
      });
  };

  const confirm = (user) => {
    return r
      .table('users')
      .get(user.id)
      .update({
        emailConfirmed: true,
        perishableToken: null
      })
      .then((result) => {
        user.emailConfirmed = true;
        user.perishableToken = null;
        return user;
      });
  };

  return (req, res) => {
    if (req.query.token === undefined) {
      res.status(500).send(ERR_MISSING_TOKEN);
    } else {
      fetch(req.query.token)
        .then(confirm)
        .then(notify)
        .then((user) => {
          const redirect = req.query.redirect_to || '/';
          res.redirect(redirect);
        })
        .catch((error) => {
          if (error) console.error(error);
          res.status(403).send(ERR_INVALID_TOKEN);
        });
    }    
  };

};