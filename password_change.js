const bcrypt = require('bcrypt');
const _ = require('lodash');
const fs = require('fs');
const Handlebars = require('handlebars');

const ERR_PASSWORD_GENERATION = 'Password generation error';
const ERR_UNKNOWN = "UNKNOWN ERROR";
const ERR_UNAUTHORIZED = "UNAUTHORIZED";

module.exports = (r, email, _settings) => {
  
  const settings = _.merge({    
    site: {
      name: 'NAME THIS SITE!',                                          // used for templates
      url: 'http://localhost:8000'                                      // used for link generation
    },
    emails: {
      passwordResetConfirm: {
        subject: 'Your password has been changed',
        template: Handlebars.compile(fs.readFileSync(__dirname + '/templates/password_change.html', 'utf-8'))
      }
    }
  }, _settings);

  const notify = (user) => {
    console.log('notify', user);
    return new Promise((resolve, reject) => {
      const data = {
        params: user,
        site: {
          name: settings.site.name
        }
      };
      const html = settings.emails.passwordResetConfirm.template(data);
      const message = {
        text: "",
        from: settings.email.from,
        subject: settings.emails.passwordResetConfirm.subject,
        to: user.email,
        attachment: [
          { data: html, alternative: true }
        ]
      };
      email.send(message, (error, message) => {
        if (error) {
          console.error(error);
          reject(new Error(ERR_EMAIL_NOTIFICATION));
        } else {
          resolve(user);
        }
      });
    });    
  };

  const verifyPassword = (params) => {
    return (user) => {
      return new Promise((resolve, reject) => {
        bcrypt.compare(params.currentPassword, user.password, (error, valid) => {
          console.log('password verified', valid);
          if (error || !valid) {
            reject(new Error(ERR_UNAUTHORIZED));
          } else {            
            resolve(user);
          }
        });
      });
    };
  };

  const changePassword = (params) => {
    return (user) => {
      console.log('change password', params, user);
      return new Promise((resolve, reject) => {
        if (params.newPassword.length <=6) {
          reject(new Error(ERR_UNAUTHORIZED));
        } else {
          console.log('changing', params, 'user', user);
          bcrypt.hash(params.newPassword, 13, (err, hash) => {            
            console.log('changing password to', hash);
            r.table('users')
            .get(user.id)
            .update({
              password: hash,
              updatedAt: new Date()
            })
            .then((result) => {
              resolve(user);
            })
            .catch(reject);
          });
        }
      });
    };
  };
  
  const fetch = (user) => {
    console.log('fetching user', user);
    return r
      .table('users')
      .get(user.id);
  };

  return (req, res) => {
    const params = req.body;    
    fetch(req.auth.user)
      .then(verifyPassword(params))
      .then(changePassword(params))
      .then(notify)
      .then((user) => {
        res.send({ success: true, message: "Password has been changed" });
      })
      .catch((error) => {
        console.error(error);
        res.status(500).send(ERR_UNKNOWN);
      });
  };

}