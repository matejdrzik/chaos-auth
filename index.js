const express = require('express');
const router = express.Router();
const email = require('emailjs');
const _ = require('lodash');
const bcrypt = require('bcrypt');
const validateParams = require('./validate_params');
const Register = require('./register');
const Activate = require('./activate');
const Login = require('./login');
const AdminLogin = require('./admin_login');
const Logout = require('./logout');
const PasswordReset = require('./password_reset');
const PasswordResetConfirm = require('./password_reset_confirm');
const PasswordChange = require('./password_change');
const Authorize = require('./authorize');
const CreateUser = require('./create_user');
const UpdateUser = require('./update_user');

const ERR_INSUFFICIENT_PERMS = 'Insufficient permissions';

module.exports = (r, settings) => {
  this.r = r;

  this.settings = _.merge({
    secret: 'MY_SUPER_DUPER_SECRET',
    email: {
      from: "no-reply@foo.com",
      host: "localhost",
      user: "",
      password: "",
      ssl: false
    }
  }, settings);

  this.email = email.server.connect(settings.email);
  
  this.authorize = Authorize(r);

  this.requireAdmin = (req, res, next) => {    
    if (req.auth.user.role !== 'admin') {
      res.status(403).send(ERR_INSUFFICIENT_PERMS);
    } else {
      next();
    }
  };

  this.requireSuperuser = (req, res, next) => {
    if (req.auth.user.role === 'user') {
      res.status(403).send(ERR_INSUFFICIENT_PERMS);
    } else {
      next();
    }
  };

  this.router = router;

  // account management
  this.router.post('/register', Register(r, this.email, settings));
  this.router.get('/activate', Activate(r, this.email, settings));

  // session management
  this.router.post('/login', Login(r, settings));
  this.router.post('/admin/login', this.authorize, this.requireAdmin, AdminLogin(r, settings));
  this.router.get('/logout', this.authorize, Logout(r, settings));

  // password management
  this.router.get('/password_reset', PasswordReset(r, this.email, settings));
  this.router.get('/password_reset_confirm', PasswordResetConfirm(r, this.email, settings));
  this.router.post('/password_change', this.authorize, this.requireAdmin, PasswordChange(r, this.email, settings));
  this.router.get('/', this.authorize, (req, res) => { res.send(req.auth); });
  
  // user management
  this.router.post('/users', this.authorize, this.requireSuperuser, CreateUser(r));
  this.router.put('/users/:id', this.authorize, this.requireSuperuser, UpdateUser(r));


  this.initialize = () => {
    const self = this;
    const required = { 
      users: [ "email", "perishableToken", "organization_id", "createdAt" ],
      organizations: [ "email", "createdAt" ],
      tokens: [ "token", "user_id", "organization_id", "createdAt" ]
    };    
    this.r.tableList()
     .then((tables) => {
      const requiredTables = Object.keys(required);
      const processTable = () => {
        const requiredTable = requiredTables[0];        
        if (_.includes(tables, requiredTable)) {
           // table already exists, verify indexes
          self.r
            .table(requiredTable)
            .indexList()
            .then((indexes) => {
              const requiredIndexes = required[requiredTable];

              const processIndex = () => {
                const index = requiredIndexes[0];                

                if (_.includes(indexes, index)) {
                  // index already exists, do nothing
                  requiredIndexes.shift();
                  if (requiredIndexes.length > 0) {
                    processIndex();
                  } else {
                    if (requiredTables.length > 0) {
                      processTable();
                    }
                  }
                } else {
                  // index doesnt exist, create and loop
                  console.log('should create index', index);
                  self.r.table(requiredTable)
                    .indexCreate(index)
                    .then(() => {
                      console.log('AUTH', `index ${requiredTable}/${index} created`);
                      requiredIndexes.shift();
                      if (requiredIndexes.length > 0) {
                        processIndex();
                      } else {
                        requiredTables.shift();
                        if (requiredTables.length > 0) {
                          processTable();
                        }
                      }
                    });
                }
              };
              
              if (requiredIndexes.length > 0) {
                processIndex();
              } else {
                requiredTables.shift();                
                if (requiredTables.length > 0) {
                  processTable();                  
                } else {
                  // all done in this point
                }                
              }
            });
        } else {
          // table does not exist yet, create table and indexes
          self.r.tableCreate(requiredTable)
           .then(() => {
            console.log('AUTH', `table ${requiredTable} created`);
            const requiredIndexes = required[requiredTable];
            const processIndex = () => {
              const index = requiredIndexes[0];              
              // index doesnt exist, create and loop
              self.r.table(requiredTable)
                .indexCreate(index)
                .then(() => {
                  console.log('AUTH', `index ${requiredTable}/${index} created`);
                  requiredIndexes.shift();
                  if (requiredIndexes.length > 0) {
                    processIndex();
                  } else {
                    requiredTables.shift();
                    if (requiredTables.length > 0) {
                      processTable();
                    }
                  }
                });
            };

            processIndex();
           });
        }
      };
      processTable();
     });  
  };

  this.initialize();  

  return this;
};
