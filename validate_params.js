module.exports = (params, list) => {
  const valid = [];
  list.forEach((param) => {    
    if (params[param]) {
      valid.push(param);
    } else {
      console.log('missing param', param, 'from', list);
    }
  });  
  return valid.length == list.length;  
};