const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const r = require('rethinkdbdash')({ db: 'chaostest' });

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

const settings = {
  secret: 'MY_SUPER_DUPER_SECRET_LOLIPOP',
  site: {
    name: 'Cool Site',
    url: 'http://localhost:3000'
  },
  email: {
    from: "no-reply@foo.com",
    host: "mail.somewhere.com",
    user: "foo@bar.com",
    password: "somepassword",
    ssl: true
  }
};

const app = express();
 
app.use(cors());
app.use(bodyParser.json())
   .use(bodyParser.urlencoded({ extended: false }));
app.use((error, req, res, next) => {
  res.status(500).send(error.message);    
});

const auth = require('./index')(r, settings);

app.use('/auth', auth.router);

app.get('/test', auth.authorize, auth.requireAdmin, (req, res) => {
  console.log('test works');
  res.send({ success: true, message: 'i am authorized' });
});

app.listen(3000);
